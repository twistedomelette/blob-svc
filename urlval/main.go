package urlval

import (
	"net/url"
	"reflect"

	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

const urlvalKey = "url"

func parseFieldTag(values url.Values) map[string]interface{} {
	figValues := make(map[string]interface{})
	for k, v := range values {
		figValues[k] = v[0]
	}
	return figValues
}

func Decode(values url.Values, dest interface{}) error {
	rval := reflect.Indirect(reflect.ValueOf(dest))
	rtyp := rval.Type()
	for fi := 0; fi < rval.NumField(); fi++ {
		value := rval.Field(fi)
		field := rtyp.Field(fi)

		err := figure.Out(&dest).With(figure.BaseHooks).From(parseFieldTag(values)).SetField(value, field, urlvalKey)
		if err != nil {
			return errors.Wrap(err, "failed to decode field", logan.F{"field": field.Name})
		}

		values.Del(field.Tag.Get(urlvalKey))
	}
	return nil
}
