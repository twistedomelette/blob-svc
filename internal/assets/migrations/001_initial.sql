-- +migrate Up

CREATE TABLE blobs
(
    id serial not null primary key,
    owner_id varchar(255) not null,
    name jsonb not null
);

-- +migrate Down

DROP TABLE blobs;