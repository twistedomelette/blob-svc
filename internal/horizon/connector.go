package horizon

import (
	"gitlab.com/tokend/connectors/horizon"
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/keypair"
)

type Connector struct {
	*horizon.Connector

	signer keypair.Full
	source keypair.Address

	accountSignerId  uint64
	accountSignerKey string
}

func NewConnector(connector *horizon.Connector, signer keypair.Full, source keypair.Address, accountManager string) *Connector {
	return &Connector{
		Connector:        connector,
		signer:           signer,
		source:           source,
		accountSignerKey: accountManager,
		accountSignerId:  1,
	}
}

func (c *Connector) TxBuilder() (*xdrbuild.Builder, error) {
	const passphrase string = "TokenD Developer Network"
	const txExpire int64 = 601200
	return xdrbuild.NewBuilder(passphrase, txExpire), nil
}
