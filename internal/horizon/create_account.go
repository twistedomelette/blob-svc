package horizon

import (
	"context"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/connectors/submit"
	"gitlab.com/tokend/go/keypair"
	"gitlab.com/tokend/go/xdr"
	"gitlab.com/tokend/go/xdrbuild"
)

type VoidDetails struct{}

func (c *Connector) CreateAccount(keyPair *keypair.Full, roleId uint64, builder xdrbuild.Builder, submitter submit.Submitter) error {
	tx := builder.Transaction(c.source)

	address := keyPair.Address()

	tx = tx.Op(&xdrbuild.CreateAccount{
		Destination: address,
		RoleID:      roleId,
		Signers: []xdrbuild.SignerData{
			{
				PublicKey: c.accountSignerKey,
				RoleID:    c.accountSignerId,
				Weight:    1000,
				Identity:  1,
				Details:   &VoidDetails{},
			},
			{
				PublicKey: address,
				RoleID:    3,
				Weight:    1000,
				Identity:  0,
				Details:   &VoidDetails{},
			},
		},
	})

	envelope, err := tx.Sign(c.signer).Marshal()
	if err != nil {
		return errors.Wrap(err, "failed to build tx envelope")
	}

	resultXDR, err := submitForRawResult(envelope, submitter)
	if err != nil {
		return errors.Wrap(err, "failed to submit tx")
	}

	var txResult xdr.TransactionResult
	if err = xdr.SafeUnmarshalBase64(resultXDR, &txResult); err != nil {
		return errors.Wrap(err, "failed to unmarshal result")
	}

	if txResult.Result.Code != 0 {
		return errors.Wrap(errors.New("transaction got negative result code"), txResult.Result.Code.String())
	}

	return nil
}

func submitForRawResult(env string, submitter submit.Submitter) (string, error) {
	result, err := submitter.Submit(context.Background(), env, false, false)
	if err != nil {
		if txFailure, ok := err.(submit.TxFailure); ok {
			return txFailure.ResultXDR, err
		}
		return "", err
	}
	return result.Data.Attributes.ResultXdr, nil
}

func (VoidDetails) MarshalJSON() ([]byte, error) {
	return []byte(`{}`), nil
}
