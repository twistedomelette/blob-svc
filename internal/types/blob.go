package types

import "encoding/json"

type Blob struct {
	Id      uint64
	Name    json.RawMessage
	OwnerId *Owner `db:"owner_id"`
}

type Owner string
