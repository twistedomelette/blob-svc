package config

import (
	"gitlab.com/tokend/blob-svc/internal/horizon"
	"net/http"
	"net/url"

	"gitlab.com/distributed_lab/figure"
	"gitlab.com/distributed_lab/kit/kv"
	"gitlab.com/distributed_lab/logan/v3/errors"
	abstract "gitlab.com/tokend/connectors/horizon"
	"gitlab.com/tokend/connectors/signed"
	"gitlab.com/tokend/keypair/figurekeypair"
)

type HorizonConfig struct {
	Endpoint *url.URL `fig:"endpoint"`
}

func (c *config) HorizonConnector() *horizon.Connector {
	return c.horizonConnector.Do(func() interface{} {
		var config HorizonConfig

		err := figure.
			Out(&config).
			With(figure.BaseHooks, figurekeypair.Hooks).
			From(kv.MustGetStringMap(c.getter, "horizon")).
			Please()
		if err != nil {
			panic(errors.Wrap(err, "failed to figure out horizon"))
		}

		signer := c.Keyer.Keys().Signer

		cli := signed.NewClient(http.DefaultClient, config.Endpoint)
		if signer != nil {
			cli = cli.WithSigner(signer)
		}
		connector := abstract.NewConnector(cli, signer)
		if connector == nil {
			panic("connector is nil")
		}

		return horizon.NewConnector(connector, signer, c.Keyer.Keys().Source, c.Keyer.Keys().Signer.Address())
	}).(*horizon.Connector)
}
