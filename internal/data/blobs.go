package data

import (
	"gitlab.com/tokend/blob-svc/internal/types"
)

type BlobsQ interface {
	New() BlobsQ
	Create(blob *types.Blob) error
	Get(id string) (*types.Blob, error)
	Select() ([]types.Blob, error)
	Delete(id string) error
	Page(page uint64) BlobsQ
	FilterByOwnerId(ownerId string) BlobsQ
}
