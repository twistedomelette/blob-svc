package postgres

import (
	"database/sql"
	"github.com/Masterminds/squirrel"
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/tokend/blob-svc/internal/data"
	"gitlab.com/tokend/blob-svc/internal/types"
)

const (
	blobsTable      = "blobs"
	tableBlobsLimit = 3
)

var (
	blobsSelect = squirrel.
		Select("id", "owner_id", "name").
		From(blobsTable)
)

type Blobs struct {
	*pgdb.DB
	stmt squirrel.SelectBuilder
}

func (q *Blobs) New() data.BlobsQ {
	return NewBlobs(q.DB)
}

func NewBlobs(repo *pgdb.DB) *Blobs {
	return &Blobs{
		repo.Clone(), blobsSelect,
	}
}

func (q *Blobs) Create(blob *types.Blob) error {
	stmt := squirrel.Insert(blobsTable).SetMap(map[string]interface{}{
		"id":       blob.Id,
		"owner_id": *blob.OwnerId,
		"name":     blob.Name,
	})

	err := q.Exec(stmt)

	return err
}

func (q *Blobs) Get(id string) (*types.Blob, error) {
	var result types.Blob

	stmt := q.stmt.Where("id = ?", id)

	err := q.DB.Get(&result, stmt)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return &result, nil
}

func (q *Blobs) Select() ([]types.Blob, error) {
	var result []types.Blob

	err := q.DB.Select(&result, q.stmt)

	if err != nil {
		if err == sql.ErrNoRows {
			return nil, nil
		}
		return nil, err
	}
	return result, nil
}

func (q *Blobs) Delete(id string) error {
	stmt := squirrel.
		Delete("blobs").
		Where(squirrel.Eq{"id": id})

	err := q.Exec(stmt)

	return err
}

func (q *Blobs) Page(page uint64) data.BlobsQ {
	q.stmt = q.stmt.Offset(tableBlobsLimit * (page - 1)).Limit(tableBlobsLimit)
	return q
}

func (q *Blobs) FilterByOwnerId(ownerId string) data.BlobsQ {
	q.stmt = q.stmt.Where(squirrel.Eq{"owner_id": ownerId})
	return q
}
