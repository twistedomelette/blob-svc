package requests

import (
	"encoding/json"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/blob-svc/internal/types"
	"gitlab.com/tokend/blob-svc/resources"
	"gitlab.com/tokend/blob-svc/urlval"
	"net/http"
)

func NewCreateBlobRequest(r *http.Request) (resources.Blob, error) {
	var request resources.BlobResponse

	if err := json.NewDecoder(r.Body).Decode(&request); err != nil {
		return request.Data, errors.Wrap(err, "failed to unmarshal")
	}

	return request.Data, nil
}

func NewGetBlobFilters(r *http.Request) (GetBlobFilters, error) {
	request := GetBlobFilters{}

	err := urlval.Decode(r.URL.Query(), &request)
	if err != nil {
		return request, err
	}

	return request, nil
}

type GetBlobFilters struct {
	Page    uint64 `url:"page[number]"`
	OwnerId string `url:"filter[owner-id]"`
}

func Blob(r resources.Blob) (*types.Blob, error) {
	var newBlob types.Blob
	json.Unmarshal([]byte(r.Attributes.Value), &newBlob)
	ownerId := types.Owner(r.Relationships.Owner.Data.ID)

	return &types.Blob{
		Id:      newBlob.Id,
		Name:    newBlob.Name,
		OwnerId: &ownerId,
	}, nil
}
