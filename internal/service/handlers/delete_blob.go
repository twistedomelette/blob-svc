package handlers

import (
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"net/http"
)

func DeleteBlobRequest(r *http.Request) (GetBlobRequest, error) {
	request := GetBlobRequest{
		BlobID: chi.URLParam(r, "blob"),
	}
	return request, nil
}

func DeleteBlob(w http.ResponseWriter, r *http.Request) {
	request, err := DeleteBlobRequest(r)

	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	err = BlobQ(r).Delete(request.BlobID)

	if err != nil {
		Log(r).WithError(err).Error("failed to delete invites")
		return
	}

	w.WriteHeader(http.StatusNoContent)
}
