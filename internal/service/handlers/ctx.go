package handlers

import (
	"context"
	"gitlab.com/tokend/blob-svc/internal/data"
	"gitlab.com/tokend/blob-svc/internal/horizon"
	"net/http"

	"gitlab.com/distributed_lab/logan/v3"
)

type ctxKey int

const (
	logCtxKey ctxKey = iota
	blobQCtxKey
	horizonCtxKey
)

func CtxLog(entry *logan.Entry) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, logCtxKey, entry)
	}
}

func Log(r *http.Request) *logan.Entry {
	return r.Context().Value(logCtxKey).(*logan.Entry)
}

func CtxBlobQ(q data.BlobsQ) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, blobQCtxKey, q)
	}
}

func BlobQ(r *http.Request) data.BlobsQ {
	return r.Context().Value(blobQCtxKey).(data.BlobsQ).New()
}

func CtxHorizon(horizon *horizon.Connector) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, horizonCtxKey, horizon)
	}
}

func Horizon(r *http.Request) *horizon.Connector {
	return r.Context().Value(horizonCtxKey).(*horizon.Connector)
}
