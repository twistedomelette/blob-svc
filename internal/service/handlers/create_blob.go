package handlers

import (
	validation "github.com/go-ozzo/ozzo-validation/v4"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/blob-svc/internal/service/requests"
	"net/http"
)

func CreateBlob(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewCreateBlobRequest(r)

	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	blob, err := requests.Blob(request)

	if err != nil {
		Log(r).WithError(err).Warn("invalid blob type")
		ape.RenderErr(w, problems.BadRequest(validation.Errors{"/data/type": errors.New("invalid blob type")})...)
		return
	}

	err = BlobQ(r).Create(blob)

	if err != nil {
		Log(r).WithError(err).Error("failed to save blob")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	w.WriteHeader(201)
	ape.Render(w, blob)
}
