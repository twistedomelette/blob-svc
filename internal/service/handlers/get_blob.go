package handlers

import (
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"gitlab.com/tokend/blob-svc/internal/service/requests"
	"gitlab.com/tokend/go/keypair"
	"net/http"
)

type GetBlobRequest struct {
	BlobID string
}

func NewGetBlobRequest(r *http.Request) (GetBlobRequest, error) {
	request := GetBlobRequest{
		BlobID: chi.URLParam(r, "blob"),
	}
	return request, nil
}

func GetBlob(w http.ResponseWriter, r *http.Request) {
	log := Log(r).WithField("tag", "blob_performance")
	log.Info("Request started")
	request, err := NewGetBlobRequest(r)

	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	log.Info("Try to get blob from DB")

	blob, err := BlobQ(r).Get(request.BlobID)

	if err != nil {
		Log(r).WithError(err).Error("failed to get blob")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	if blob == nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	log.Info("Render response")

	ape.Render(w, blob)
}

func GetBlobsByOwnerId(w http.ResponseWriter, r *http.Request) {
	log := Log(r).WithField("tag", "blob_performance")
	log.Info("Request started")

	request, err := requests.NewGetBlobFilters(r)

	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	blobs, err := BlobQ(r).
		FilterByOwnerId(request.OwnerId).
		Page(request.Page).
		Select()

	if blobs == nil {
		ape.RenderErr(w, problems.NotFound())
		return
	}

	if err != nil {
		Log(r).WithError(err).Error("failed to get blobs")
		ape.RenderErr(w, problems.InternalError())
		return
	}

	log.Info("Render response")

	ape.Render(w, blobs)
}

func SendTransaction(w http.ResponseWriter, r *http.Request) {
	keyPair, err := keypair.Random()
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}
	h := Horizon(r)
	builder, _ := h.TxBuilder()
	err = h.CreateAccount(keyPair, 3, *builder, *h.Submitter)
	if err != nil {
		ape.RenderErr(w, problems.InternalError())
		return
	}
	return
}
