package service

import (
	"gitlab.com/tokend/blob-svc/internal/data/postgres"
	"gitlab.com/tokend/blob-svc/internal/service/handlers"

	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
)

func (s *service) router() chi.Router {
	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxBlobQ(postgres.NewBlobs(s.DB)),
			handlers.CtxHorizon(s.horizon),
		),
	)
	r.Route("/integrations/blob-send", func(r chi.Router) {
		r.Get("/", handlers.SendTransaction)
	})
	r.Route("/integrations/blob-svc", func(r chi.Router) {
		// configure endpoints here
		r.Get("/{blob}", handlers.GetBlob)
		r.Get("/", handlers.GetBlobsByOwnerId)
		r.Delete("/{blob}", handlers.DeleteBlob)
		r.Post("/", handlers.CreateBlob)
	})

	return r
}
