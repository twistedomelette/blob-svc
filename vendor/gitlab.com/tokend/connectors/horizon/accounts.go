package horizon

import (
	horizon "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/logan/v3/errors"
	regources "gitlab.com/tokend/regources/generated"
)

const accountsUrl = "/v3/accounts"

func (c *Connector) Accounts(query ...Query) ([]regources.Account, *regources.Included, error) {
	var response regources.AccountListResponse

	err := c.GetList(accountsUrl, &response, query...)
	if err != nil {
		if err == horizon.ErrNotFound {
			return []regources.Account{}, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting accounts")
	}

	return response.Data, &response.Included, err
}

func (c *Connector) AccountById(id string, query ...Query) (*regources.Account, *regources.Included, error) {
	var response regources.AccountResponse

	err := c.GetOne(&BasicPath{
		Id:  id,
		Url: accountsUrl,
	}, &response, query...)

	if err != nil {
		if err == horizon.ErrNotFound {
			return nil, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting account")
	}

	return &response.Data, &response.Included, nil
}
