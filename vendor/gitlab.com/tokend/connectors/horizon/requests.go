package horizon

import (
	horizon "gitlab.com/distributed_lab/json-api-connector"
	"gitlab.com/distributed_lab/logan/v3/errors"
	regources "gitlab.com/tokend/regources/generated"
)

const requestsUrl = "/v3/requests"

func (c *Connector) Requests(query ...Query) ([]regources.ReviewableRequest, *regources.Included, error) {
	var response regources.ReviewableRequestListResponse

	err := c.GetList(requestsUrl, &response, query...)
	if err != nil {
		if err == horizon.ErrNotFound {
			return []regources.ReviewableRequest{}, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting requests")
	}

	return response.Data, &response.Included, nil
}

func (c *Connector) RequestById(id string, query ...Query) (*regources.ReviewableRequest, *regources.Included, error) {
	var response regources.ReviewableRequestResponse

	err := c.GetOne(&BasicPath{
		Id:  id,
		Url: requestsUrl,
	}, &response, query...)

	if err != nil {
		if err == horizon.ErrNotFound {
			return nil, nil, nil
		}
		return nil, nil, errors.Wrap(err, "error getting request")
	}

	return &response.Data, &response.Included, nil
}
